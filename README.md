# Description
This is a scientific look up table editor which generates customized colormaps for pseudocolor imaging purpose. Actual it only supports a tonal chromatic scale because i use it in a differnt project to map audio frequencies to colors... 

## Dependencies
This programm uses qt, see: [qt-project.org](http://qt-project.org/)

For color map generation it uses qwt, see: [qwt](http://qwt.sourceforge.net/)

## Build steps
```qmake```

```make```

