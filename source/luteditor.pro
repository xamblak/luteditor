QT += core gui

greaterThan(QT_MAJOR_VERSION, 4){
    QT += widgets
}

UI_DIR = uics
MOC_DIR = mocs
OBJECTS_DIR = objs

TARGET = luteditor
TEMPLATE = app

CONFIG += qwt

SOURCES += main.cpp\
        luteditor.cpp \
        lookuptable.cpp

HEADERS  += luteditor.h \
        lookuptable.h

FORMS    += luteditor.ui
