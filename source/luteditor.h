/*
 * File: luteditor.h
 * Date of creation: 2013/4/14
 *
 * Copyright (C) 2013 Xam Blak
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef LUTEDITOR_H
#define LUTEDITOR_H


#include <qwt_color_map.h>

#include <QWidget>
#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QColorDialog>

enum CHORDTYPE {
    MAJ = 0,
    MIN = 1,
    QUI = 2,
    CHR = 3,
    cnt = 4
};

namespace Ui {
class LutEditor;
}

class LutEditor : public QWidget
{
    Q_OBJECT
    
public:
    explicit LutEditor(QWidget *parent = 0);
    ~LutEditor();

    static QMap<double, QColor> initializeColorStops();
    static QMap<double, QColor> modifyInitialColorStops(QMap<double, QColor> chromaSteps, CHORDTYPE type);

    static void generateLut(QwtLinearColorMap *map, QMap<double, QColor> colors);
    static QPixmap drawLutPixmap(QwtLinearColorMap *cMap, int sizeX = 512, int sizeY = 200, QList<double> ticks = QList<double>(), bool drawTicks = true);

protected:
     bool eventFilter(QObject *object, QEvent *event);
     void resizeEvent(QResizeEvent *event);
    
private:
    Ui::LutEditor *ui;

    QwtLinearColorMap colorMap;
    QMap<double, QColor> colorStops;

    QString imageFileName;
    QGraphicsScene *graphicsSceneLut;
    QColorDialog *colorChooser;
    QPixmap lutPixmap;
    QSize drawLUTSize;
    int selectedIdx;


    void printColorStops(QMap<double, QColor> stops);
    int getColorIndex(double position, QList<double> ticks);
    QString getChordType(CHORDTYPE type);

private slots:
    void chordChanged(int chordIdx);
    void changeSelectedColor(QColor actColor);
    void redrawLUT(bool ticksOn);
    void saveImage();
};

#endif // LUTEDITOR_H
