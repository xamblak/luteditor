/*
 * File: luteditor.cpp
 * Date of creation: 2013/4/14
 *
 * Copyright (C) 2013 Xam Blak
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <math.h>

#include "luteditor.h"
#include "ui_luteditor.h"

#include <QGraphicsSceneMouseEvent>
#include <QFileDialog>

#include <iostream>

using namespace std;

LutEditor::LutEditor(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LutEditor)
{
    ui->setupUi(this);


    drawLUTSize.setWidth(512);
    drawLUTSize.setHeight(200);

    // Initialize color stops
    colorStops = initializeColorStops();

    // Modify stops to be major chord tones
    colorStops = modifyInitialColorStops(colorStops, MIN);

    for(int i=0; i<(int)cnt; i++)
    {
        ui->cbChooseLut->addItem(getChordType((CHORDTYPE)i));

    }
    ui->cbChooseLut->setCurrentIndex((int)MIN);

    connect(ui->cbChooseLut, SIGNAL(currentIndexChanged(int)), this, SLOT(chordChanged(int)));

    //printColorStops(colorStops);

    // Initialize lut up table
    generateLut(&colorMap, colorStops);

    // Initialize lut pixmap
    lutPixmap = drawLutPixmap(&colorMap, drawLUTSize.width(), drawLUTSize.height(), colorStops.keys(), ui->cbDrawTicks->isChecked());

    graphicsSceneLut = new QGraphicsScene();
    ui->graphicsView->setScene(graphicsSceneLut);

    graphicsSceneLut->addPixmap(lutPixmap);
    graphicsSceneLut->installEventFilter(this);

    colorChooser = new QColorDialog();

    connect(colorChooser, SIGNAL(currentColorChanged(QColor)), this, SLOT(changeSelectedColor(QColor)));
    connect(ui->cbDrawTicks, SIGNAL(toggled(bool)), this, SLOT(redrawLUT(bool)));
    connect(ui->pbSave, SIGNAL(clicked()), this, SLOT(saveImage()));
}

/*
 * Destructor
 */
LutEditor::~LutEditor()
{
    delete ui;
}

/*
 * Returns color stops at all chromatic positions [0..1] using HSV color wheel (30° steps).
 */
QMap<double, QColor> LutEditor::initializeColorStops()
{
    QMap<double, QColor> map;

    // Calculate halftone positions
    QVector<double> halftonePositions;
    halftonePositions.push_front(0);

    map.insert(0.0, QColor(255,0,0));

    for(int i=1; i<13; i++)
    {
        halftonePositions.push_front( 1-(pow(2, i/(double)12)-1) );
        QColor color(0,0,0);

        if( i != 12 )
        {
            color.setHsv(360-(360*i/12), 255, 255);
            map.insert(halftonePositions.at(0), color);
        }
    }

    map.insert(1.0, QColor(255,0,0));

    return map;
}


/*
 * Removes all elements which are not nesseccary for the given CHORDTYPE and returns the modification.
 * Note: There have to be 13 elements inside the chromaSteps otherwise it will be returned unmodified.
 */
QMap<double, QColor> LutEditor::modifyInitialColorStops(QMap<double, QColor> chromaSteps, CHORDTYPE type )
{
    // Each chromatic position which is 0 gets deleted
    int useTones[13] = {1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1};


    QMap<double, QColor> output(chromaSteps);

    if( chromaSteps.size() != 13 )
        return output;

    switch( type )
    {
        case MAJ: //         1           3        5              1'
        {         //         0 01 02 03 04 05 06 07 08 09 10 11 12
            int tones[13] = {1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1};
            memcpy(useTones, tones, 13*sizeof(int));
            break;
        }
        case MIN: //         1        3           5              1'
        {         //         0 01 02 03 04 05 06 07 08 09 10 11 12
            int tones[13] = {1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1};
            memcpy(useTones, tones, 13*sizeof(int));
            break;
        }
        case QUI: //         1                    5              1'
        {         //         0 01 02 03 04 05 06 07 08 09 10 11 12
            int tones[13] = {1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1};
            memcpy(useTones, tones, 13*sizeof(int));
            break;
        }
        case CHR: //         1  ..                               1'
        {         //         0 01 02 03 04 05 06 07 08 09 10 11 12
            int tones[13] = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
            memcpy(useTones, tones, 13*sizeof(int));
            break;
        }
        default:  //         1  ..                               1'
        {         //         0 01 02 03 04 05 06 07 08 09 10 11 12
            int tones[13] = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
            memcpy(useTones, tones, 13*sizeof(int));
            break;
        }
    }

    int counter = 0;

    // Delete map entries which are 0
    for( QMap<double, QColor>::iterator it = output.begin(); it != output.end(); )
    {
        if( useTones[counter] == 0 )
        {
            it = output.erase(it);
        }
        else ++it;
        counter++;
    }

    return output;
}

/*
 * Returns a string corresponding to the CHORDTYPE.
 */
QString LutEditor::getChordType(CHORDTYPE type)
{
    QString chord = "unknown";
    switch( type )
    {
    case MAJ: chord = "major"; break;
    case MIN: chord = "minor"; break;
    case QUI: chord = "quint"; break;
    default:  chord = "chromatic"; break;
    }
    return chord;
}

/*
 * Resets the LUT and changes the amount of color ticks depending on the given chords index.
 */
void LutEditor::chordChanged(int chordIdx)
{
    colorStops = initializeColorStops();
    colorStops = modifyInitialColorStops(colorStops, (CHORDTYPE)chordIdx);

    generateLut(&colorMap, colorStops);
    redrawLUT(ui->cbDrawTicks->isChecked());
}

/*
 * Returns a pixmap containing the given LUT with size X x Y.
 *
 * Optional the given ticks are drawn above the color map (LUT).
 */
QPixmap LutEditor::drawLutPixmap(QwtLinearColorMap *cMap, int sizeX, int sizeY, QList<double> ticks, bool drawTicks)
{
    QwtInterval interval;
    interval.setInterval(0, sizeX);

    // Initialize lut pixmap
    QPixmap pixmap = QPixmap(sizeX, sizeY);

    // Draw lut into pixmap
    QPainter painter( &pixmap );
    for(int x=0; x<sizeX; x++)
    {
        painter.setPen( QPen(cMap->color(interval, x)) );
        painter.drawLine(x,0,x,sizeY);
    }

    // Draw overlay ticks
    if( drawTicks )
    {
        for(int i=0; i<ticks.size(); i++)
        {
            int tickPos = (int)(sizeX-1-ticks.at(i)*(sizeX-1)) + 1;
            painter.setPen(QPen(QColor(0,0,0, 125)));
            painter.drawLine(sizeX-tickPos,0,sizeX-tickPos,sizeY);
        }
    }

    return pixmap;
}


/*
 * Initilaizes the given qwt color map with the given colors map.
 *
 * Where the QMaps keys are the tick positions {0..1} and values are the color at these ticks.
 */
void LutEditor::generateLut(QwtLinearColorMap *map, QMap<double, QColor> colors)
{
    QColor start(255,0,0);
    QColor stop(0,0,255);

    if( colors.size() >= 1 )
    {
        start = colors.begin().value();
        stop = (colors.end()-1).value();
    }

    map->setColorInterval(start, stop);
    map->setMode(QwtLinearColorMap::ScaledColors);

    QMapIterator<double, QColor> i(colors);
    while( i.hasNext() )
    {
        i.next();
        map->addColorStop(i.key(), i.value());
    }
}

/*
 * Prints each colors position and the rgb values to std out.
 */
void LutEditor::printColorStops(QMap<double, QColor> stops)
{
    QMapIterator<double, QColor> i(stops);
    while( i.hasNext() )
    {
        i.next();
        cout << i.key() << "\t: " << i.value().red() << "\t, " << i.value().green() << "\t, " << i.value().blue() << endl;
    }
}

/*
 * Used to handle mouse click events for the graphicsSceneLut.
 *
 * Recognizes the clicked color tick and opens a color chooser dialog to select a color and applies it to the LUT.
 */
bool LutEditor::eventFilter(QObject *object, QEvent *event)
{
    if (object == graphicsSceneLut && event->type() == QEvent::GraphicsSceneMousePress )
    {
        QGraphicsSceneMouseEvent *mouseEvent = static_cast<QGraphicsSceneMouseEvent *>(event);
        QPointF position = mouseEvent->scenePos();


        if( position.x() > 0 && position.x() < drawLUTSize.width() )
        {
            double realPosX = position.x() / (double)drawLUTSize.width();
            int index = getColorIndex(realPosX, colorStops.keys());
            //cout << "pos: " << position.x() << ", " << realPosX << ", idx = " << index << endl;

            selectedIdx = index;

            colorChooser->setCurrentColor((colorStops.begin()+index).value());

            if( colorChooser->exec() == QDialog::Accepted )
            {
                changeSelectedColor( colorChooser->currentColor() );
                return true;
            }
        }
    }

    return false;
}


/*
 * Always resizes the graphicsSceneLUT to the actual size of the ui's graphicsView.
 */
void LutEditor::resizeEvent(QResizeEvent *event)
{
    ui->graphicsView->fitInView(graphicsSceneLut->itemsBoundingRect() ,Qt::IgnoreAspectRatio);
    QWidget::resizeEvent(event);
}


/*
 * Replaces the color at selectedIdx with the given color and redraws the LUT.
 */
void LutEditor::changeSelectedColor(QColor actColor)
{
    colorStops[(colorStops.begin()+selectedIdx).key()] = actColor;

    // Fix highest and lowest color to same
    if( ui->cbSwap->isChecked() )
    {
        if( selectedIdx == 0 )
        {
            colorStops[(colorStops.begin()+colorStops.size()-1).key()] = colorStops.begin().value();
        }

        if( selectedIdx == colorStops.size()-1 )
        {
            colorStops[colorStops.begin().key()] = (colorStops.begin()+colorStops.size()-1).value();
        }
    }

    generateLut(&colorMap, colorStops);
    redrawLUT(ui->cbDrawTicks->isChecked());
}


/*
 * Redraws the lutPixmap with respect to ui's cbDrawTicks.
 */
void LutEditor::redrawLUT(bool ticksOn)
{
    lutPixmap = drawLutPixmap(&colorMap, drawLUTSize.width(), drawLUTSize.height(), colorStops.keys(), ticksOn);//ui->cbDrawTicks->isChecked());

    // Inefficient: Instead addPixmap there should be updatePixmap
    // Workaround: To avoid mem leak, always remove all items before adding.
    QList<QGraphicsItem*> items = graphicsSceneLut->items();
    for (int i = 0; i < items.size(); i++) {
        graphicsSceneLut->removeItem(items[i]);
        delete items[i];
    }
    graphicsSceneLut->addPixmap(lutPixmap);
}


/*
 * Open save file dialog to let the user choose and location to save the LUT as image.
 * Note -> Workaround: Switches off ticks drawing temporarely.
 */
void LutEditor::saveImage()
{
    imageFileName = QDir::homePath();
    imageFileName = QFileDialog::getSaveFileName(NULL, tr("Save Iamge"),
                                                 imageFileName + "/untitled.png",
                                                 tr("Images (*.png .jpg)"));

    // TODO: Use custom image size
    lutPixmap = drawLutPixmap(&colorMap, 512, 200, colorStops.keys(), false);

    if( imageFileName.isEmpty() != !lutPixmap.save(imageFileName) )
    {
        cerr << "Save file " << imageFileName.toStdString() << " failed!" << endl;
    }

    lutPixmap = drawLutPixmap(&colorMap, drawLUTSize.width(), drawLUTSize.height(), colorStops.keys(), ui->cbDrawTicks->isChecked());
}


/*
 * Returns the index of the nearest tick to the given position
 */
int LutEditor::getColorIndex(double position, QList<double> ticks)
{
    int index = -1;

    if( ticks.size() < 2 )
        return index;

    double min = 100;

    for(int i=0; i<ticks.size(); i++)
    {
        double distance = fabs(position - ticks.at(i));
        if( distance < min )
        {
            min = distance;
            index = i;
        }
    }

    return index;
}
